-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2021 at 11:56 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_gmtalk`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `phone`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `receipt`, `date_created`, `date_updated`) VALUES
(1, '1640775062', '9fca0047bce845e8011b14974fb3c48a', NULL, NULL, NULL, NULL, 'User 1', '0111231231', 'user1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zxo0iib7', NULL, NULL, 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-12-29 10:51:02', '2021-12-29 10:51:07'),
(2, '1640775337', '5ef630b291f3c7f7b9dab8f4d61d19e0', NULL, NULL, NULL, NULL, 'User 2', '0111231232', 'user2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'wmrsauyb', NULL, NULL, 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2021-12-29 10:55:37', '2021-12-29 10:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `service_date` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `service_type`, `service_date`, `original_price`, `final_price`, `discount`, `totalPrice`, `status`, `date_created`, `date_updated`) VALUES
(1, '5ef630b291f3c7f7b9dab8f4d61d19e0', NULL, 'GD M-12', NULL, '1640775337', NULL, NULL, NULL, NULL, '88', NULL, NULL, NULL, '2021-12-29 10:55:37', '2021-12-29 10:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_holder` varchar(255) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `preorder_list`
--

CREATE TABLE `preorder_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `service_date` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `diamond` varchar(255) DEFAULT NULL,
  `commission` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `category`, `brand`, `name`, `sku`, `slug`, `price`, `diamond`, `commission`, `status`, `description`, `keyword_one`, `link`, `image_one`, `image_two`, `image_three`, `date_created`, `date_updated`) VALUES
(1, '5bb64ea7cf11fb667e676f905a978066', 'Hydrogen', 'Hydrogen', 'GD M-12', NULL, NULL, '88', NULL, NULL, 'Available', 'GD6K', NULL, NULL, '1609335574gn6k.jpeg', NULL, NULL, '2020-12-16 09:01:24', '2021-12-29 10:11:53'),
(2, 'd1c43df2f897eefa72b6b281b0274ac7', 'Hydrogen', 'Hydrogen', 'GN M-12', NULL, NULL, '99', NULL, NULL, 'Available', 'GN5K', NULL, NULL, '1609335595gn5k.jpeg', NULL, NULL, '2020-12-16 09:02:05', '2021-12-29 10:12:23'),
(3, '6e8587ace9d0d2d9c4fa96fb7c065d29', 'Hydrogen', 'Hydrogen', 'GDN Mix', NULL, NULL, '111', NULL, NULL, 'Available', 'GDN Mix', NULL, NULL, '1610436175hydrogen_mask.jpeg', NULL, NULL, '2021-01-12 07:22:55', '2021-12-29 10:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(255) DEFAULT NULL COMMENT 'For login probably if needed',
  `email` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `user_type` int(2) DEFAULT NULL COMMENT '0 = admin, 1 = normal user',
  `remark_one` varchar(255) DEFAULT NULL,
  `remark_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `fullname`, `company_name`, `duration`, `manufactured`, `expired`, `status`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '528d6cb31e16abb36c1365094e5c9183', 'admin', 'admin@gmail.com', '074bb3bdb8b3791688fb4af87b339aeea0c2867b7f12df8b47f35f7384396624', '9de0f39a26c88b978964c426c9fc89db0f66028a', '012123123', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-06 03:55:51', '2020-11-09 09:26:04'),
(2, 'ea6182d593b150753933c58b01040bd6', 'user', 'user@gmail.com', '074bb3bdb8b3791688fb4af87b339aeea0c2867b7f12df8b47f35f7384396624', '9de0f39a26c88b978964c426c9fc89db0f66028a', '011445566', 'user', 'Test Company', NULL, NULL, NULL, NULL, 1, '2020-11-06 04:00:21', '2020-11-11 07:27:10'),
(3, '9654080b675074125dc45a6699a7d3de', 'oliver', 'oliver@gmail.com', '802deb1dcaa4e04933e45e217df0daa8bf2b379a666d29d5761975a02d554bb4', 'b0144665481863b8a681f389bf38c1f55fe5eafc', '011123123', 'oliver', 'Queen Consolidated', '365 days', '2020-11-11', '2021-11-11', 'Approved', 2, '2020-11-09 09:25:46', '2020-11-11 08:11:46'),
(5, '476bc8b154abbb53dec8d8bc68ac98a7', 'barry', 'barry@gmail.com', '7694adb6fc142eb623249d2935873559affc3f87f71a42270c4890925ec57b48', '1cfdcdbfd2b94fb0eabac56ffa81ec0d93c83430', '0127744520', 'barry', 'Star Labs', NULL, NULL, NULL, NULL, 1, '2020-11-11 07:19:34', '2020-11-11 07:19:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preorder_list`
--
ALTER TABLE `preorder_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preorder_list`
--
ALTER TABLE `preorder_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
