-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2021 at 09:56 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_gmtalk`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_holder` varchar(255) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(255) DEFAULT NULL COMMENT 'For login probably if needed',
  `email` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `user_type` int(2) DEFAULT NULL COMMENT '0 = admin, 1 = normal user',
  `remark_one` varchar(255) DEFAULT NULL,
  `remark_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `fullname`, `user_type`, `remark_one`, `remark_two`, `date_created`, `date_updated`) VALUES
(1, NULL, NULL, '0123432535', NULL, NULL, 'tester@gmail.com', 'tester', NULL, NULL, NULL, '2021-12-22 08:50:44', '2021-12-22 08:50:44'),
(2, '038726aa8a24049b0882a58072764629', NULL, '01845151558', NULL, NULL, 'user@gmail.com', 'user', NULL, NULL, NULL, '2021-12-22 08:53:28', '2021-12-22 08:53:28');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `manufactured` varchar(255) DEFAULT NULL,
  `expired` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `fullname`, `company_name`, `duration`, `manufactured`, `expired`, `status`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '528d6cb31e16abb36c1365094e5c9183', 'admin', 'admin@gmail.com', '074bb3bdb8b3791688fb4af87b339aeea0c2867b7f12df8b47f35f7384396624', '9de0f39a26c88b978964c426c9fc89db0f66028a', '012123123', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-06 03:55:51', '2020-11-09 09:26:04'),
(2, 'ea6182d593b150753933c58b01040bd6', 'user', 'user@gmail.com', '074bb3bdb8b3791688fb4af87b339aeea0c2867b7f12df8b47f35f7384396624', '9de0f39a26c88b978964c426c9fc89db0f66028a', '011445566', 'user', 'Test Company', NULL, NULL, NULL, NULL, 1, '2020-11-06 04:00:21', '2020-11-11 07:27:10'),
(3, '9654080b675074125dc45a6699a7d3de', 'oliver', 'oliver@gmail.com', '802deb1dcaa4e04933e45e217df0daa8bf2b379a666d29d5761975a02d554bb4', 'b0144665481863b8a681f389bf38c1f55fe5eafc', '011123123', 'oliver', 'Queen Consolidated', '365 days', '2020-11-11', '2021-11-11', 'Approved', 2, '2020-11-09 09:25:46', '2020-11-11 08:11:46'),
(5, '476bc8b154abbb53dec8d8bc68ac98a7', 'barry', 'barry@gmail.com', '7694adb6fc142eb623249d2935873559affc3f87f71a42270c4890925ec57b48', '1cfdcdbfd2b94fb0eabac56ffa81ec0d93c83430', '0127744520', 'barry', 'Star Labs', NULL, NULL, NULL, NULL, 1, '2020-11-11 07:19:34', '2020-11-11 07:19:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
