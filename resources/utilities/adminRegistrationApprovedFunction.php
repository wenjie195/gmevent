<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Registration.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $userUid = rewrite($_POST["user_uid"]);

    $status = "Approved";

    $registrationDetails = getRegistration($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");   

    if($registrationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"remark_one");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"registration"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminRegistrationApproved.php?type=1');
        }
        else
        {
            echo "FAIL";
        }
    }
    else
    {
        echo "ERROR"; 
    }

}
else 
{
    header('Location: ../index.php');
}
?>