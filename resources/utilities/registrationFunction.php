<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Registration.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

// function registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType,$fullname,$remarkOne)
// {
//      if(insertDynamicData($conn,"registration",array("uid","username","phone_no","email","user_type","fullname","remark_one"),
//           array($uid,$username,$phoneNo,$email,$userType,$fullname,$remarkOne),"ssssiss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function registerNewUser($conn,$uid,$username,$phoneNo,$email,$fullname,$remarkOne)
{
     if(insertDynamicData($conn,"registration",array("uid","username","phone_no","email","fullname","remark_one"),
          array($uid,$username,$phoneNo,$email,$fullname,$remarkOne),"ssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function sendEmailForVerification($uid)
{
     $conn = connDB();
     $userRows = getRegistration($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.vincaps.com";
     $verifyUser_usernameThatSendEmail = "noreply@vincaps.com";                   // Sender Acc Username
     $verifyUser_password = "8dQLllnG1yh5";                                              // Sender Acc Password

     $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
     $verifyUser_port = 465;                                                              // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@vincaps.com";                    // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@vincaps.com";                        // Sender Email


     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                            // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Vincaps Registration Confirmation";

     $verifyUser_body = "<p>Dear ".$userRows[0]->getUsername().",</p>";                      // Body
     $verifyUser_body .="<p>This is your voucher details:</p>";
     // $verifyUser_body .="<p>https://vincaps.com/</p>";
     $verifyUser_body .="<p>http://localhost/vincaps/ticketReview.php?uid=".$userRows[0]->getUid()."</p>  ";
     // $verifyUser_body .="<p>https://vincaps.com/ticketReview.php?uid=".$userRows[0]->getUid()."</p>  ";
     $verifyUser_body .="<p>Voucher Code : ".$userRows[0]->getUid()."</p>";
     $verifyUser_body .="<p>Thank you.</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());
     $uid = uniqid();

     $username = rewrite($_POST['name']);
     $fullname = rewrite($_POST['name']);
     $email = rewrite($_POST['email']);
     $phoneNo = rewrite($_POST['phone']);
     // $userType = "1";
     $remarkOne = "Pending";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";

     // $usernameRows = getRegistration($conn," WHERE username = ? ",array("username"),array($username),"s");
     // $usernameDetails = $usernameRows[0];

     $usernameRows = getRegistration($conn," WHERE email = ? AND phone_no = ?",array("email","phone_no"),array($email,$phoneNo),"ss");
     $usernameDetails = $usernameRows[0];

     if (!$usernameDetails)
     {
          if(registerNewUser($conn,$uid,$username,$phoneNo,$email,$fullname,$remarkOne))
          {
               // echo"Success";
               sendEmailForVerification($uid);
               $_SESSION['registration_uid'] = $uid;
               // $_SESSION['messageType'] = 1;
               // header('Location: ../registration.php?type=1');
               // header('Location: ../ticketView.php?type=1');
               header('Location: ../ticketView.php');
          }
          else
          {
               echo"Fail";
          } 
     }
     else
     {
          // echo"Name Repeat";  
          // echo "<script>alert('Email or Phone Number has been used !<br> Please try a new one');window.location='../registration.php'</script>";  
          $_SESSION['messageType'] = 1;
          header('Location: ../registration.php?type=2');
     }
}
else 
{
     header('Location: ../livechat.php');
}
?>