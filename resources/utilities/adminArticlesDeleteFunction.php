<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["blog_uid"]);
    $display = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $display_Type."<br>";

    if(isset($_POST['blog_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $loginTypeUpdated = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        
        if($loginTypeUpdated)
        {
            // echo "success";
            header('Location: ../adminBlogView.php?type=1');
        }
        else
        {
            echo "fail";
            // header('Location: ../adminViewArticles.php?type=2');
        }
    }
    else
    {
        echo "error";
        // header('Location: ../adminViewArticles.php?type=3');
    }

}
else
{
     header('Location: ../index.php');
}
?>