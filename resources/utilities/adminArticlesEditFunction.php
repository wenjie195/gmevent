<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];
$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $blogUid = rewrite($_POST['blog_uid']);

     $title = rewrite($_POST['update_title']);

     $articleLink = rewrite($_POST['update_article_link']);

     $keywordTwo = rewrite($_POST['update_keyword']);

     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;

     // $author = rewrite($_POST['update_author']);
     // $str = str_replace( array(' ',','), '', $title);
     // $strkeywordOne = str_replace( array(','), '', $articleLink);
     // $AddToEnd = "-";
     // $seoTitle = $str.$AddToEnd.$strkeywordOne;
     // $type = rewrite($_POST['update_type']);

     // $file = $timestamp.$_FILES['file_one']['name'];

     $coverPhoto = $timestamp.$_FILES['update_cover_photo']['name'];
     $target_dir = "../uploadsArticle/";
     $target_file = $target_dir . basename($_FILES["update_cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_cover_photo']['tmp_name'],$target_dir.$coverPhoto);
     }

     // $imgCoverSrc = rewrite($_POST['update_cover_photo_source']);
     $imgCoverSrcData = rewrite($_POST['update_photo_source']);
     if($imgCoverSrcData ==  '')
     {
          $imgCoverSrc = "No";
     }
     else
     {
          $imgCoverSrc = rewrite($_POST['update_photo_source']);
     }

     $keywordOne = rewrite($_POST['update_summary']);
     $paragraphOne = ($_POST['editor']);  //no rewrite, cause error in db

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($articleLink)
          {
               array_push($tableName,"article_link");
               array_push($tableValue,$articleLink);
               $stringType .=  "s";
          }
          if($keywordTwo)
          {
               array_push($tableName,"keyword_two");
               array_push($tableValue,$keywordTwo);
               $stringType .=  "s";
          }
          if($seoTitle)
          {
               array_push($tableName,"seo_title");
               array_push($tableValue,$seoTitle);
               $stringType .=  "s";
          }
          if($coverPhoto)
          {
               array_push($tableName,"title_cover");
               array_push($tableValue,$coverPhoto);
               $stringType .=  "s";
          }
          if($imgCoverSrc)
          {
               array_push($tableName,"img_cover_source");
               array_push($tableValue,$imgCoverSrc);
               $stringType .=  "s";
          }
          if($keywordOne)
          {
               array_push($tableName,"keyword_one");
               array_push($tableValue,$keywordOne);
               $stringType .=  "s";
          }
          if($paragraphOne)
          {
               array_push($tableName,"paragraph_one");
               array_push($tableValue,$paragraphOne);
               $stringType .=  "s";
          }
          
          array_push($tableValue,$blogUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               header('Location: ../adminBlogView.php?type=1');
          }
          else
          {    
               echo "FAIL";
               // header('Location: ../adminBlogView.php?type=1');
          }
     }
     else
     {
          echo "ERROR";
          // header('Location: ../viewArticles.php?type=3');
     }
}
else 
{
     header('Location: ../index.php');
}

?>