<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$fullname,$email,$phoneNo,$finalPassword,$salt,$companyName,$username)
{
     if(insertDynamicData($conn,"user",array("uid","fullname","email","phone_no","password","salt","company_name","username"),
          array($uid,$fullname,$email,$phoneNo,$finalPassword,$salt,$companyName,$username),"ssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     if(isset($_POST['register']))
     {
     
          $uid = md5(uniqid());
          // $username = rewrite($_POST['register_username']);
          $fullname = rewrite($_POST['register_fullname']);
          $username = $fullname;
          $email = rewrite($_POST['register_email']);
          $phoneNo = rewrite($_POST['register_contact']);
          $companyName = rewrite($_POST['register_company_name']);

          $register_password = rewrite($_POST['register_password']);
          $register_password_validation = strlen($register_password);
          $register_retype_password = rewrite($_POST['register_retype_password']);
          $password = hash('sha256',$register_password);
          $salt = substr(sha1(mt_rand()), 0, 100);
          $finalPassword = hash('sha256', $salt.$password);
     
          //   FOR DEBUGGING 
          // echo "<br>";
          // echo $uid."<br>";
          // echo $username."<br>";
          // echo $email."<br>";
          // echo $phoneNo ."<br>";
     
          $fullnameRows = getUser($conn," WHERE fullname = ? ",array("fullname"),array($fullname),"s");
          $fullnameDetails = $fullnameRows[0];
     
          $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($email),"s");
          $userEmailDetails = $userEmailRows[0];
     
          $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($phoneNo),"s");
          $userPhoneDetails = $userPhoneRows[0];
     
          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if (!$fullnameDetails)
                    {
                         if (!$userPhoneDetails)
                         {
                              if (!$userEmailDetails)
                              {
                                   if(registerNewUser($conn,$uid,$fullname,$email,$phoneNo,$finalPassword,$salt,$companyName,$username))
                                   {
                                        // echo "<script>alert('Register Success !');window.location='../index.php'</script>";    
                                        // echo "Register Success";
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../index.php?type=1');
                                   }
                                   else
                                   {
                                        // $_SESSION['messageType'] = 1;
                                        // header('Location: ../index.php?type=2');
                                        echo "fail to register";
                                   }
                              }
                              else
                              {
                                   echo "email has been taken by others";
                              }
                         }
                         else
                         {
                              echo "phone has been taken by others";
                         }
                    }
                    else
                    {
                         echo "name has been taken by others";
                    }
               }
               else 
               {
                    echo "<script>alert('password must be more than 5');window.location='../index.php'</script>";
               }
          }
          else 
          {
               echo "<script>alert('password is different with retype password');window.location='../index.php'</script>";
          } 

          // if (!$fullnameDetails)
          // {
          //      if (!$userPhoneDetails)
          //      {
          //           if (!$userEmailDetails)
          //           {
          //                if(registerNewUser($conn,$uid,$username,$email,$phoneNo))
          //                {
          //                     // echo "<script>alert('Register Success !');window.location='../index.php'</script>";    
          //                     echo "Register Success";
          //                }
          //                else
          //                {
          //                     $_SESSION['messageType'] = 1;
          //                     header('Location: ../index.php?type=2');
          //                }
          //           }
          //           else
          //           {
          //                echo "email has been taken by others";
          //           }
          //      }
          //      else
          //      {
          //           echo "phone has been taken by others";
          //      }
          // }
          // else
          // {
          //      echo "name has been taken by others";
          // }
          
     }

     $conn->close();

}
else 
{
     header('Location: ../index.php');
}
?>