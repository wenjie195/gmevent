<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/packageRegistration.php" />
<link rel="canonical" href="https://vincaps.com/packageRegistration.php" />
<meta property="og:title" content="Registration" />
<title>Registration</title>

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1" style="padding-top:70px !important;">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Add New Event</h1>

        <div class="five-col float-left two-column-css">

        <!-- <form method="POST" action="utilities/adminRegistrationApprovedFunction.php"> -->
        <form method="POST" action="utilities/adminProductAddFunction.php" enctype="multipart/form-data">

            <input type="text" placeholder="Event Name" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" required>

            <input type="text" placeholder="Event Description" id="description" name="description" class="input-name clean dark-blue-text wow fadeIn" required>

            <input type="text" placeholder="Event Price" id="price" name="price" class="input-name clean dark-blue-text wow fadeIn" required>

            <div class="clear"></div>

            <input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" required/>

            <div class="clear"></div>

            <div class="res-div">
                <input type="submit" name="submit" value="Proceed To Payment" class="input-submit blue-button white-text clean pointer lato wow fadeIn" style="margin-bottom:30px;">
            </div>
        </form>
            
        </div>	
</div>

<div class="clear"></div>  

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Event Added !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail to add new event !"; 
        }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Mobile Number or Email already existing, Pls try again !";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>