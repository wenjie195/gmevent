<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Registration.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $conn = connDB();
// $conn->close();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = md5(uniqid());
    $name = rewrite($_POST["name"]);
    $mobile = rewrite($_POST["mobile"]);
    $email = rewrite($_POST["email"]);

    $timestamp = time();

    function registerNewUser($conn,$uid,$name,$mobile,$email)
    {
        if(insertDynamicData($conn,"registration",array("uid","fullname","phone_no","email"),
        array($uid,$name,$mobile,$email),"ssss") === null)
        {
            echo "gg";
        }
        else{    }
        return true;
    }

    if(registerNewUser($conn,$uid,$name,$mobile,$email))
    {}
}
// else
// {
//     date_default_timezone_set('Asia/Kuala_Lumpur');
//     $dateOnly = date('Y-m-d', time());
// }

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/registration.php" />
<link rel="canonical" href="https://vincaps.com/registration.php" />
<meta property="og:title" content="VinCaps | Registration" />
<title>VinCaps | Registration</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1" style="padding-top:70px !important;">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Confirm Your Register Details</h1>

        <div class="five-col float-left two-column-css">

        <form method="post" action="billplzpost.php">
            <input type="text" value="<?php echo $name;?>" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" readonly >
            <input type="text" type="text" value="<?php echo $mobile;?>" id="mobile" name="mobile" class="input-name clean dark-blue-text wow fadeIn" readonly >
            <input type="email" value="<?php echo $email;?>" id="email" name="email" class="input-name clean dark-blue-text wow fadeIn" readonly >
        
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="RM99" readonly> 

            <input class="input-name clean" type="hidden" id="amount" name="amount" value="9900" readonly> 

            <input class="input-name clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order UID" readonly> 
            <input class="input-name clean" type="hidden" id="reference_1" name="reference_1" value="<?php echo $uid;?>" readonly>
            <input class="input-name clean" type="hidden" id="reference_2_label" name="reference_2_label" value="Order ID" readonly> 
            <input class="input-name clean" type="hidden" id="reference_2" name="reference_2" value="<?php echo $timestamp;?>" readonly> 

            <div class="res-div">
                <input type="submit" name="submit" value="Proceed To Payment" class="input-submit blue-button white-text clean pointer lato wow fadeIn" style="margin-bottom:30px;"></div>
        </form>
            
        </div>	
</div>

<div class="clear"></div>  

<!-- <div class="width100 overflow">
</div> -->

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>

<?php include 'js.php'; ?>
<?php unset($_SESSION['registration_uid']);?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>