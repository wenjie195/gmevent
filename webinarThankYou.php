<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/gmevent/webinarThankYou.php" />
<link rel="canonical" href="https://vincaps.com/gmevent/webinarThankYou.php" />
<meta property="og:title" content="谢谢报名文案图设计速成班 | 光明日报" />
<title>谢谢报名文案图设计速成班 | 光明日报</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<img src="img/left-leaf.png" class="left-leaf">
<div class="width100 bg3 minheight-with-footer">
    <div class="big-container-css width100 ow-big-container-css">
      <div class="blue-div-opa width100 small-padding">
      	<!--<span class="close-css inner-close  upper-close">&times;</span>-->
      	<img src="img/thank-you.png" class="hello-icon blue-icon2" alt="Thank You" title="Thank You">
      	<h1 class="white-text welcome lato welcome2">谢谢报名</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     
    
            <p class="lato blue-text explanation-p text-center thankyou-p">你将会在近日收到我们的电子邮件，请留意你的邮箱！<br>若有疑问可拨打+60 16-418 3692</p>
        	<a href="index.php"><div class="input-submit green-bg-change white-text clean pointer lato below-forgot text-center" >回到主页</div></a>

    </div>
  </div>
  <img src="img/right-leaf2.png" class="right-leaf2" style="right:0 !important; bottom:50px !important; position:absolute !important;">
</div>

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}

</style>

<?php include 'js.php'; ?>
<?php unset($_SESSION['timestamp']);?>

</body>
</html>