<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $userDetails = getUser($conn," WHERE user_type = 1");
$products = getProduct($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminProductAll.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminProductAll.php" />
    <meta property="og:title" content="<?php echo _PRODUCT ?> | Hygenie Group" />
    <title><?php echo _PRODUCT ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<!-- <div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly"> -->
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1" style="padding-top:70px !important;">

    <!-- <p class="white-text p-title"><b>All Product</b></p> -->

    <h1 class="small-h1-a text-center white-text">All Event | <a class="blue-link" href="adminProductAdd.php">Add New Event</a> </h1>

    <div class="clear"></div>

    <div class="overflow-scroll-div">
        <table class="table-css fix-th tablesorter smaller-font-table">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Event Name</th>
                    <th>Price (RM)</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($products)
                    {
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $products[$cnt]->getName();?></td>
                                <td><?php echo $products[$cnt]->getPrice();?></td>

                                <td>
                                    <form action="utilities/adminProductDeleteFunction.php" method="POST" class="hover1">
                                        <button class="clean blue-ow-btn" type="submit" name="product_uid" value="<?php echo $products[$cnt]->getUid();?>">
                                           Delete
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div class="clear"></div>

    <div class="width100 bottom-spacing"></div>

</div>

<?php include 'js.php'; ?>

<script src="js/headroom.js"></script>

<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Product Details Updated. ";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Product Details!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "ERROR";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Product Deleted. ";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Delete Product Details!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "ERROR";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Product Added. ";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Add New Product!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Prodcut Name Has Been Used <br> Please Check";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>

</body>
</html>