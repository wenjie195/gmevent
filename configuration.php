<?php
/**
 * Instruction:
 *
 * 1. Replace the APIKEY with your API Key.
 * 2. OPTIONAL: Replace the COLLECTION with your Collection ID.
 * 3. Replace the X_SIGNATURE with your X Signature Key
 * 4. Replace the http://www.google.com/ with your FULL PATH TO YOUR WEBSITE. It must be end with trailing slash "/".
 * 5. Replace the http://www.google.com/success.html with your FULL PATH TO YOUR SUCCESS PAGE. *The URL can be overridden later
 * 6. OPTIONAL: Set $amount value.
 * 7. OPTIONAL: Set $fallbackurl if the user are failed to be redirected to the Billplz Payment Page.
 *
 */
// $api_key = 'APIKEY';
// $collection_id = 'COLLECTION';
// $x_signature = 'X_SIGNATURE';

// sandbox
$api_key = 'dc8b2c92-96ac-45e7-bd2b-df7628729562';
$collection_id = 'COLLECTION';
$x_signature = 'S-v1Q2xGC5QXikPIw_VguOHg';

// // server
// $api_key = 'b61564bc-1a55-424e-b13b-ac4536fb33b6';
// $collection_id = 'COLLECTION';
// $x_signature = 'S-Ey7SRpmkf_N_rlPNmcexsg';

// $websiteurl = 'http://www.google.com';
// $successpath = 'http://www.google.com/success.html';
// $amount = ''; //Example (RM13.50): $amount = '1350';
// $fallbackurl = ''; //Example: $fallbackurl = 'http://www.google.com/pay.php';
// $description = 'PAYMENT DESCRIPTION';
// $reference_1_label = '';
// $reference_2_label = '';

$path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$fullPath = dirname($path);

//localhost
$websiteurl = 'http://localhost/gmevent';
$successpath = 'http://localhost/gmevent/webinarThankYou.php';

// //server
// $websiteurl = "https://".$fullPath;
// $successpath = $websiteurl.'/webinarThankYou.php';

$amount = ''; //Example (RM13.50): $amount = '1350';
$fallbackurl = ''; //Example: $fallbackurl = 'http://www.google.com/pay.php';
$description = 'PAYMENT DESCRIPTION';
// $reference_1_label = '';
// $reference_2_label = '';
$reference_1_label = 'Product';
$reference_2_label = 'Order ID';