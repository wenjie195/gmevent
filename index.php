<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$currentPackage = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
$packageDetails = $currentPackage[0];


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/gmevent/" />
<link rel="canonical" href="https://vincaps.com/gmevent/" />
<meta property="og:title" content="文案图速成班 | 光明日报" />
<title>文案图速成班 | 光明日报</title>

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

	<img src="img/right-leaf.png" class="right-leaf right-leaf1 wow fadeIn" data-wow-delay="0.2s" >
   <div class="width100 same-padding overflow text-center">
   	<img src="img/laptop.png" class="laptop wow fadeIn" data-wow-delay="0.4s">
    <h1 class="black-text first-h1 wow fadeIn" data-wow-delay="0.6s">就算设计基础为<b>零</b><br>自己也能轻松花<b>10分钟</b><br><b>设计</b>出吸睛的文案图！</h1>
   </div> 
   <img src="img/left-leaf.png" class="left-leaf left-leaf1 wow fadeIn" data-wow-delay="0.8s"> 
   <div class="width100 same-padding overflow text-center">
   	<p class="black-text r-u wow fadeIn" data-wow-delay="1s">你是否面临一下状况？</p>
    <img src="img/down.png" class="down-png wow  bounce animated" data-wow-iteration="infinite" data-wow-duration="16s">
   </div> 
   <div class="width100 same-padding margin-top-zz">
   		<div class="left-div1">
        	<table class="two-column-table black-text wow bounceInLeft" data-wow-delay="0.2s">
            	<tr>
                	<td><img src="img/cross.png" class="cross-png"></td>
                    <td>花了很多精力设计的文案图又<b>没人看？</b></td>
                </tr>
            	<tr>
                	<td><img src="img/cross.png" class="cross-png"></td>
                    <td>设计师做出的设计<b>不符合</b>你心目中所想?</td>
                </tr>                     
            </table>
        </div>
   		<div class="right-div1">
        	<table class="two-column-table black-text wow  bounceInRight" data-wow-delay="0.2s">
            	<tr>
                	<td><img src="img/tick2.png" class="cross-png"></td>
                    <td>刚踏入网络营销想要经营<b>自己生意的社群？</b></td>
                </tr>
            	<tr>
                	<td><img src="img/tick2.png" class="cross-png"></td>
                    <td><b>设计图自己做绝不出错</b>且不会花费太长时间， 只要你用对formula！</td>
                </tr>                     
            </table>
        </div>        
        
   </div>
    <div class="clear"></div>
   <div class="width100 same-padding overflow text-center padding-z">
   	<img src="img/design.png" class="design wow  pulse  animated" data-wow-iteration="infinite" data-wow-duration="8s">
   	<p class="black-text r-u wow fadeIn" data-wow-delay="0.2s">你只需要加入以下课程，<br>不求人也一样可以做出专业的文案图！</p>
    <img src="img/down2.png" class="down-png wow  bounce animated" data-wow-iteration="infinite" data-wow-duration="16s">
   </div>     
    
<!--    <div class="width100 same-padding padding-top-menu">
    	<div class="five-div-container">
        	<div class="five-div">
            	<img src="img/icon1.png" class="five-div-img wow fadeIn" data-wow-delay="0.2s">
                <p class="five-div-p black-text wow fadeIn" data-wow-delay="0.4s">刚踏入网络营销, 想要经营自己生意的社群？</p>
            </div>
        	<div class="five-div center-five">
            	<img src="img/icon2.png" class="five-div-img wow fadeIn" data-wow-delay="0.6s">
                <p class="five-div-p black-text wow fadeIn" data-wow-delay="0.8s">花了很多精力去设计自己的文案图但又没人看？</p>
            </div>            
        	<div class="five-div">
            	<img src="img/icon3.png" class="five-div-img wow fadeIn" data-wow-delay="1s">
                <p class="five-div-p black-text wow fadeIn" data-wow-delay="1.2s">跟设计师沟通了但做出的图案不是你心目中想要的图案...</p>
            </div>             
        	<div class="five-div">
            	<img src="img/icon4.png" class="five-div-img wow fadeIn" data-wow-delay="1.4s">
                <p class="five-div-p black-text wow fadeIn" data-wow-delay="1.6s">所以<b>设计图自己做</b>， 重要的是设计不需要花费你很多时间，只要你用对formula！</p>
            </div>   
        	<div class="five-div center-five">
            	<img src="img/icon5.png" class="five-div-img wow fadeIn" data-wow-delay="1.8s">
                <p class="five-div-p black-text wow fadeIn" data-wow-delay="2s"><b>不求人</b>，也一样可以<b>做出专业的文案图！</b></p>
            </div>             
                     
        </div>
    </div>
	<div class="clear"></div>-->
    <div class="width100 bg1 overflow same-padding">
    	<div class="white-bg-div">
        	<div class="white-border-div">
                <div class="right-date wow  bounceInRight" data-wow-delay="0.2s">
                    <h1 class="date-h1 darkblue-text">25 Feb</h1>
                    <div class="h1-border"></div>
                    <h1 class="date-h1b darkblue-text">2022</h1>
                    <h1 class="darkgreen-text place-h1"><img src="img/gps.png" class="gps">ZOOM</h1>
                </div>
                <div class="left-details wow bounceInLeft" data-wow-delay="0.2s">
                    <h1 class="details-h1  darkblue-text">从基础到精通<br>文案图设计速成班</h1>
                   
                </div>
    	<div class="clear"></div>
        	<div class="text-center three-counter three-counter2a">
            	<img src="img/book.png" class="five-div-img wow fadeIn" data-wow-delay="0.4s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="0.6s">专门为文案图设计0基础的你 “特调”的文案图设计课程。
</p>
             
            </div>
        	<div class="text-center three-counter three-counter2a">
            	<img src="img/art.png" class="five-div-img wow fadeIn" data-wow-delay="0.8s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="1s">轻松学会制作好的内容、 受众的文案图、文字色彩搭配等并了解更多的设计秘诀。</p>
            
            </div>            
        	<div class="text-center three-counter three-counter2a">
            	<img src="img/group.png" class="five-div-img wow fadeIn" data-wow-delay="1.2s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="1.4s">帮你打造有凝聚力的社群平台，推广让更多人认识和记得你！</p>
            
            </div>                        
            </div>
        </div>
    </div>
	<div class="clear"></div>
    <img src="img/left-leaf.png" class="left-leaf">
    <div class="width100 same-padding">
    	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s">开课倒数计时</h1>
    	<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script>
var mq = window.matchMedia( "(max-width: 650px)" );
if (mq.matches) {
var ringer = {
  //countdown_to: "10/31/2014",
  countdown_to: "02/25/2022",
  rings: {
    '天': { 
      s: 86400000, // mseconds in a day,
      max: 365
    },
    '小时': {
      s: 3600000, // mseconds per hour,
      max: 24
    },
    '分钟': {
      s: 60000, // mseconds per minute
      max: 60
    },

    
   },
  r_count: 3,
  r_spacing: 10, // px
  r_size: 75, // px
  r_thickness: 2, // px
  update_interval: 11, // ms
    
    
  init: function(){
   
    $r = ringer;
    $r.cvs = document.createElement('canvas'); 
    
    $r.size = { 
      w: ($r.r_size + $r.r_thickness) * $r.r_count + ($r.r_spacing*($r.r_count-1)), 
      h: ($r.r_size + $r.r_thickness) 
    };
    


    $r.cvs.setAttribute('width',$r.size.w);           
    $r.cvs.setAttribute('height',$r.size.h);
    $r.ctx = $r.cvs.getContext('2d');
    $(document.body).append($r.cvs);
    $r.cvs = $($r.cvs);    
    $r.ctx.textAlign = 'center';
    $r.actual_size = $r.r_size + $r.r_thickness;
    $r.countdown_to_time = new Date($r.countdown_to).getTime();
    $r.cvs.css({ width: $r.size.w+"px", height: $r.size.h+"px" });
    $r.go();
  },
  ctx: null,
  go: function(){
    var idx=0;
    
    $r.time = (new Date().getTime()) - $r.countdown_to_time;
    
    
    for(var r_key in $r.rings) $r.unit(idx++,r_key,$r.rings[r_key]);      
    
    setTimeout($r.go,$r.update_interval);
  },
  unit: function(idx,label,ring) {
    var x,y, value, ring_secs = ring.s;
    value = parseFloat($r.time/ring_secs);
    $r.time-=Math.round(parseInt(value)) * ring_secs;
    value = Math.abs(value);
    
    x = ($r.r_size*.5 + $r.r_thickness*.5);
    x +=+(idx*($r.r_size+$r.r_spacing+$r.r_thickness));
    y = $r.r_size*.5;
    y += $r.r_thickness*.5;

    
    // calculate arc end angle
    var degrees = 360-(value / ring.max) * 360.0;
    var endAngle = degrees * (Math.PI / 180);
    
    $r.ctx.save();

    $r.ctx.translate(x,y);
    $r.ctx.clearRect($r.actual_size*-0.5,$r.actual_size*-0.5,$r.actual_size,$r.actual_size);

    // first circle
    $r.ctx.strokeStyle = "rgba(128,128,128,0.2)";
    $r.ctx.beginPath();
    $r.ctx.arc(0,0,$r.r_size/2,0,2 * Math.PI, 2);
    $r.ctx.lineWidth =$r.r_thickness;
    $r.ctx.stroke();
   
    // second circle
    $r.ctx.strokeStyle = "#c55cff";
    $r.ctx.beginPath();
    $r.ctx.arc(0,0,$r.r_size/2,0,endAngle, 1);
    $r.ctx.lineWidth =$r.r_thickness;
    $r.ctx.stroke();
    
    // label
    $r.ctx.fillStyle = "#c55cff";
   
    $r.ctx.font = '10px Helvetica';
    $r.ctx.fillText(label, 0, 23);
    $r.ctx.fillText(label, 0, 23);   
    
    $r.ctx.font = 'bold 32px Helvetica';
    $r.ctx.fillText(Math.floor(value), 0, 10);
    
    $r.ctx.restore();
  }
}

ringer.init();
}
else {
var ringer = {
  //countdown_to: "10/31/2014",
  countdown_to: "2/25/2022",
  rings: {
    '天': { 
      s: 86400000, // mseconds in a day,
      max: 365
    },
    '小时': {
      s: 3600000, // mseconds per hour,
      max: 24
    },
    '分钟': {
      s: 60000, // mseconds per minute
      max: 60
    },
    '秒': {
      s: 1000,
      max: 60
    },
    
   },
  r_count: 4,
  r_spacing: 10, // px
  r_size: 130, // px
  r_thickness: 5, // px
  update_interval: 11, // ms
    
    
  init: function(){
   
    $r = ringer;
    $r.cvs = document.createElement('canvas'); 
    
    $r.size = { 
      w: ($r.r_size + $r.r_thickness) * $r.r_count + ($r.r_spacing*($r.r_count-1)), 
      h: ($r.r_size + $r.r_thickness) 
    };
    


    $r.cvs.setAttribute('width',$r.size.w);           
    $r.cvs.setAttribute('height',$r.size.h);
    $r.ctx = $r.cvs.getContext('2d');
    $(document.body).append($r.cvs);
    $r.cvs = $($r.cvs);    
    $r.ctx.textAlign = 'center';
    $r.actual_size = $r.r_size + $r.r_thickness;
    $r.countdown_to_time = new Date($r.countdown_to).getTime();
    $r.cvs.css({ width: $r.size.w+"px", height: $r.size.h+"px" });
    $r.go();
  },
  ctx: null,
  go: function(){
    var idx=0;
    
    $r.time = (new Date().getTime()) - $r.countdown_to_time;
    
    
    for(var r_key in $r.rings) $r.unit(idx++,r_key,$r.rings[r_key]);      
    
    setTimeout($r.go,$r.update_interval);
  },
  unit: function(idx,label,ring) {
    var x,y, value, ring_secs = ring.s;
    value = parseFloat($r.time/ring_secs);
    $r.time-=Math.round(parseInt(value)) * ring_secs;
    value = Math.abs(value);
    
    x = ($r.r_size*.5 + $r.r_thickness*.5);
    x +=+(idx*($r.r_size+$r.r_spacing+$r.r_thickness));
    y = $r.r_size*.5;
    y += $r.r_thickness*.5;

    
    // calculate arc end angle
    var degrees = 360-(value / ring.max) * 360.0;
    var endAngle = degrees * (Math.PI / 180);
    
    $r.ctx.save();

    $r.ctx.translate(x,y);
    $r.ctx.clearRect($r.actual_size*-0.5,$r.actual_size*-0.5,$r.actual_size,$r.actual_size);

    // first circle
    $r.ctx.strokeStyle = "rgba(128,128,128,0.2)";
    $r.ctx.beginPath();
    $r.ctx.arc(0,0,$r.r_size/2,0,2 * Math.PI, 2);
    $r.ctx.lineWidth =$r.r_thickness;
    $r.ctx.stroke();
   
    // second circle
    $r.ctx.strokeStyle = "#c55cff";
    $r.ctx.beginPath();
    $r.ctx.arc(0,0,$r.r_size/2,0,endAngle, 1);
    $r.ctx.lineWidth =$r.r_thickness;
    $r.ctx.stroke();
    
    // label
    $r.ctx.fillStyle = "#c55cff";
   
    $r.ctx.font = '16px Helvetica';
    $r.ctx.fillText(label, 0, 33);
    $r.ctx.fillText(label, 0, 33);   
    
    $r.ctx.font = 'bold 50px Helvetica';
    $r.ctx.fillText(Math.floor(value), 0, 15);
    
    $r.ctx.restore();
  }
}

ringer.init();
}


</script>
	</div>
	<div class="clear"></div>
    <div class="width100 same-padding">
        <p class="text-center content-p-size black-text wow fadeIn" data-wow-delay="0.2s">两小时掌握【不求人】设计秘诀</p>
        <a href="#register"><div class="green-bg-change middle-button white-text wow fadeIn" data-wow-delay="0.2s">立即报名</div></a>
    </div>
    <img src="img/right-leaf2.png" class="right-leaf2">
    <img src="img/left-big-leaf.png" class="left-leaf left-leaf2">
	<div class="width100 same-padding leaf-padding-top">
    	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s">如何在竞争者中脱颖而出</h1> 
        <p class="header-p black-text wow fadeIn text-center" data-wow-delay="0.4s">一份简而有力的文案再配上设计强烈的视觉冲击，将会让大家留下深刻的印象。</p>
        <div class="clear"></div>  
 		<div class="text-center three-counter">
        	<img src="img/icon6.png" class="about-png wow fadeIn" data-wow-delay="0.6s" >
            
             <p class="mini-title black-text wow fadeIn" data-wow-delay="0.8s">两小时必备技能盘点 + 宝藏攻略</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="1s">全程干货满满， 将生硬的知识转化为大家更易懂和吸收的形式。</p>
        </div>
 		<div class="text-center three-counter">
        	<img src="img/icon7.png" class="about-png wow fadeIn" data-wow-delay="1.2s" >
            <p class="mini-title black-text wow fadeIn" data-wow-delay="1.4s">全线上教学</p>
            <p class="below-mini-title black-text wow fadeIn" data-wow-delay="1.6s">带上电脑/平板/手机就可以一起上课做笔记，不错过导师分享的精华。</p>
        </div>        
  		<div class="text-center three-counter">
        	<img src="img/icon8.png" class="about-png wow fadeIn" data-wow-delay="1.8s" >
            <p class="mini-title black-text wow fadeIn" data-wow-delay="2s">附送教学内容和视频</p>
            <p class="below-mini-title black-text wow fadeIn" data-wow-delay="2.2s">让你随时随地都可以学习和复习。另外，还会附上个邀请加入我们的【设计学习交流群】，与志同道合的朋友们一起讨论、分享、获得更多的新资讯。</p>
        </div> 
  		<div class="text-center three-counter">
        	<img src="img/icon8a.png" class="about-png wow fadeIn" data-wow-delay="2.4s" >
            <p class="mini-title black-text wow fadeIn" data-wow-delay="2.6s">免费领取《设计大礼包》</p>
            <p class="below-mini-title black-text wow fadeIn" data-wow-delay="2.8s">实战学生独家福利，等你来领取！</p>
        </div>   
  		<div class="text-center three-counter">
        	<img src="img/chat.png" class="about-png wow fadeIn" data-wow-delay="3s" >
            <p class="mini-title black-text wow fadeIn" data-wow-delay="3.2s">3个月课后服务</p>
            <p class="below-mini-title black-text wow fadeIn" data-wow-delay="3.4s">有任何问题或不懂的地方，都可以向导师发问，有问必答。</p>
        </div>         
                      
        <div class="clear"></div>
        <a href="#register"><div class="green-bg-change middle-button white-text wow fadeIn" data-wow-delay="0.2s">马上加入</div></a>	
    </div>
    <img src="img/right-big-leaf.png" class="right-leaf2 margin-top0">
    <div class="clear"></div>
	<div class="width100 same-padding leaf-padding-top">
    	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s">2小时课程你将学习到</h1> 
    	<div class="clear"></div>
        	<div class="text-center three-counter">
            	<img src="img/icon9.png" class="five-div-img wow fadeIn" data-wow-delay="0.4s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="0.6s">Canva基本功能</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="0.8s">学习Canva的工具和使用技巧，让你能够迅速熟练及运用。</p>
            </div>
        	<div class="text-center three-counter">
            	<img src="img/icon10.png" class="five-div-img wow fadeIn" data-wow-delay="1s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="1.2s">免费注册下载Canva</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="1.4s">利用Canva轻松实现你的设计需求， 数千款专业模板供你选择，为优秀设计提供了一条捷径。</p>
            </div>            
        	<div class="text-center three-counter">
            	<img src="img/icon11.png" class="five-div-img wow fadeIn" data-wow-delay="1.6s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="1.8s">设计案列分析</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="2s">通过热点设计案列，告诉你形象设计包装的策略，避开误区。</p>
            </div>             
        	<div class="text-center three-counter">
            	<img src="img/icon12.png" class="five-div-img wow fadeIn" data-wow-delay="2.2s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="2.4s">海报设计要素</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="2.6s">掌握三大关键要素：文字、产品、服务和背景，让人们一眼就能看懂你想表达什么内容。</p>
            </div>   
        	<div class="text-center three-counter">
            	<img src="img/icon13.png" class="five-div-img wow fadeIn" data-wow-delay="2.8s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="3s">色彩搭配与应用、文字选择</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="3.2s">教导你如何配色、配字可以建立更好的视觉效果呈现给观众。
</p>
            </div>  
        	<div class="text-center three-counter">
            	<img src="img/icon13a.png" class="five-div-img wow fadeIn" data-wow-delay="3.4s">
             <p class="mini-title black-text wow fadeIn" data-wow-delay="3.6s">实战演练，真学真做</p>
             <p class="below-mini-title black-text wow fadeIn" data-wow-delay="3.8s">拥有的秘诀再多不如实际应用它们，让你更深入的去了解与操作。
</p>
            </div>                        
            <div class="clear"></div> 
            
        
        <a href="#register"><div class="green-bg-change middle-button white-text wow fadeIn" data-wow-delay="0.2s">开始学习</div></a>	              
    </div>  
    <div class="width100 same-padding bg2">
    	<div class="left-img-div">
        	<img src="img/profile-img.png" class="profile-img wow fadeIn" data-wow-delay="0.2s">
        </div>
        <div class="right-lecturer-div">
        	<p class="top-h1-p white-text wow fadeIn" data-wow-delay="0.4s">你的速成班导师</p>
            <h1 class="lecturer-h1 white-text wow fadeIn" data-wow-delay="0.6s">Yap Sin Lin<br>叶欣凌</h1>
            <p class="lecturer-p white-text wow fadeIn" data-wow-delay="0.8s">使用Canva已有7年的经验，从它默默无名到现在非常普遍，推荐它给刚开始要学设计的朋友！将会结合自己在网络营梢经验与设计知识，分享设计精华指南和手把手教你如何创建有效的文案图~ 无论小白或哪个行业的老板都可以把自己行业内的专业内容分享出去，并且将你的设计技能提升到一个新的水平！
</p>
        </div>
    </div>  
    <div class="clear"></div>
    <img src="img/left-big-leaf.png" class="left-leaf left-leaf2 margin-top0">
	<div class="width100 same-padding padding-top30" id="register">
    	<h1 class="line-header wow fadeIn" data-wow-delay="0.2s">即刻报名</h1> 
        <div class="left-50div wow fadeIn" data-wow-delay="0.4s">
        	<h1 class="reg-h1 darkblue-text"><s>原价RM199</s><br>现在每位只需RM99入手<br>名额有限!</h1>
            <ul class="darkblue-text details-ul">
                        <li>0基础轻松学设计</li>
                        <li>菜鸟也能学会</li>
                        <li>容易使用,适合中小型企业老板和新手</li>
                        <li>10分钟设计出好看，设计师级别的海报</li>
                        <li>比Photoshop简单</li>
                        <li>免费注册3500+模板共你参考</li>
                        <li>名额有限，即刻就报名吧！</li>
           </ul>
        </div>
        <div class="right-50div wow fadeIn" data-wow-delay="0.6s">
            <form method="post" action="packageConfirmation.php">
    			<p class="top-p darkblue-text">姓名</p>
                <input type="text" placeholder="姓名" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" required>
                
                <p class="top-p darkblue-text">联络号码</p>
                <input type="text" type="text" placeholder="联络号码" id="mobile" name="mobile" class="input-name clean dark-blue-text wow fadeIn" required>
                <a id="mobileAlert" class="alert-a"></a>
                
                <p class="top-p darkblue-text">电邮</p>
                <input type="email" placeholder="电邮" id="email" name="email" class="input-name clean dark-blue-text wow fadeIn" required><a id="emailAlert" class="alert-a"></a>
                
                <p class="top-p darkblue-text">再次输入电邮</p>
                <input type="email" placeholder="再次输入电邮" id="reEmail" name="reEmail" class="input-name clean dark-blue-text wow fadeIn" required><a id="emailMatchAlert" class="alert-a"></a>  
                            <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="<?php echo $packageDetails->getName();;?>" id="item_name" name="item_name" readonly style="display:none;"> 
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="<?php echo $packageDetails->getPrice();;?>" id="item_price" name="item_price" readonly style="display:none;"> 
			
            <div class="res-div">
                <input type="submit" name="submit" value="确认注册" class=" width100-ow green-bg-change middle-button white-text wow fadeIn clean">
            </div>
            <p class="lato blue-text explanation-p text-center thankyou-p">若有疑问可拨打+60 16-418 3692</p>
        	</form>    
        </div>        
    </div> 
    <div class="clear"></div>   


<?php include 'js.php'; ?>

<?php unset($_SESSION['registration_uid']);?>

<script type="text/javascript">
    $("#reEmail").on("keyup",function(){
        // alert("SS");
        var email = $("#email").val();
        var reEmail = $(this).val();
        if (email == reEmail) {
            $("#emailMatchAlert").text("电邮地址吻合");
            $("#emailMatchAlert").css({
                "color":"#02c1cd",
                
            });
            $("#submit").prop("disabled",false);
        }else{
            $("#emailMatchAlert").text("请输入一样的电邮地址");
            $("#emailMatchAlert").css({
              "color":"#ff2c2c",
              
            });
            $("#submit").prop("disabled",true);
        }
    });

    $("#mobile").on("keyup",function(){
      var mobile = $(this).val();
      $.ajax({
        url: 'utilities/registerPhoneChecker.php',
        data: {mobile:mobile},
        type: 'post',
        dataType: 'json',
        success:function(response){
          var resultAlert = response[0]['result'];
          var alertBin = response[0]['alertBin'];
          if (alertBin == 0 && mobile) {
          $("#mobileAlert").text(resultAlert);
            $("#mobileAlert").css({
                "color":"#02c1cd",
               
            });
            $("#submit").prop("disabled",false);
          }else if(alertBin == 1 && mobile){
          $("#mobileAlert").text(resultAlert);
            $("#mobileAlert").css({
                "color":"#ff2c2c",
               
            });
            $("#submit").prop("disabled",true);
          }else{
            $("#mobileAlert").empty();
          }
        },
      });
    });

    $("#email").on("keyup",function(){
      var email = $(this).val();
      $.ajax({
        url: 'utilities/registerEmailChecker.php',
        data: {email:email},
        type: 'post',
        dataType: 'json',
        success:function(response){
          var resultAlert = response[0]['result'];
          var alertBin = response[0]['alertBin'];
          if (alertBin == 0 && email) {
          $("#emailAlert").text(resultAlert);
            $("#emailAlert").css({
                "color":"#02c1cd",
               
            });
            $("#submitBtn").prop("disabled",false);
          }else if(alertBin == 1 && email){
          $("#emailAlert").text(resultAlert);
            $("#emailAlert").css({
                "color":"#ff2c2c",
               
            });
            $("#submitBtn").prop("disabled",true);
          }else{
            $("#emailAlert").empty();
          }
        },
      });
    });
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        elseif($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Mobile Number or Email already existing, Pls try again !";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>


</body>
</html>