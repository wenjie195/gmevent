<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['uid'];

$conn = connDB();

// $pendingMember = getRegistration($conn);
// $pendingMember = getOrders($conn, " WHERE payment_status = 'Pending' ");
$pendingMember = getOrders($conn, " WHERE payment_status = 'Pending' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/gmevent/adminRegistrationPending.php" />
<link rel="canonical" href="https://vincaps.com/gmevent/adminRegistrationPending.php" />
<meta property="og:title" content="还未发邮件的会员" />
<title>还未发邮件的会员</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 small-padding2 min-height overflow">

  <h1 class="price-h1 dark-blue-text lato">还未发邮件的会员 | <a href="adminRegistrationApproved.php" class="light-blue-link">已发邮件的会员</a></h1>

	<div class="clear"></div>

  <div class="search-input1">
      <p class="input-top-p">搜索名字</p>
      <input class="input-name clean lato blue-text" type="text" onkeyup="myFunction()" placeholder="名字" id="myInput" name="myInput">
  </div>

  <div class="search-input1">
      <p class="input-top-p">搜索联络号码</p>
      <input class="input-name clean lato blue-text" type="text" onkeyup="myFunctionB()" placeholder="联络号码" id="myInput2" name="myInput2">
  </div>

  <div class="search-input1 search-input3">
      <p class="input-top-p">搜索电邮</p>
      <input class="input-name clean lato blue-text" type="text" onkeyup="myFunctionC()" placeholder="电邮" id="myInput3" name="myInput3">
  </div>

  <div class="search-input1">
      <p class="input-top-p">搜索付款明细</p>
      <input class="input-name clean lato blue-text" type="text" onkeyup="myFunctionD()" placeholder="付款明细" id="myInput4" name="myInput4">
  </div>

  <div class="search-input1 search-input2">
      <p class="input-top-p">搜索Zoom Code</p>
      <input class="input-name clean lato blue-text" type="text" onkeyup="myFunctionE()" placeholder="Zoom Code" id="myInput5" name="myInput5">
  </div>

	<div class="clear"></div>

    <div class="scroll-div margin-top30">
  
        <table class="approve-table lato" id="myTable">
                <thead>
                    <tr>
                        <th>序</th>
                        <th>姓名</th>
                        <th>联络号码</th>
                        <th>电邮</th>
                        <!-- <th>Item</th> -->
                        <!-- <th>Payment Amount</th> -->
                        <th>付款明细</th>
                        <th>Zoom Code</th>
                        <th>发送邮件</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($pendingMember)
                    {
                        for($cnt = 0;$cnt < count($pendingMember) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $pendingMember[$cnt]->getName();?></td>
                                <td><?php echo $pendingMember[$cnt]->getPhone();?></td>
                                <td><?php echo $pendingMember[$cnt]->getEmail();?></td>

                                <?php 
                                // $orderId = $pendingMember[$cnt]->getOrderId();
                                // $uid = $pendingMember[$cnt]->getUid();

                                // $conn = connDB();
                                // $orderDetails = getOrderList($conn," WHERE user_uid = ? AND order_id = ? ",array("user_uid","order_id"),array($uid,$orderId),"ss");
                                ?>

                                <!-- <td><?php //echo $orderDetails[0]->getProductName();?></td>
                                <td>RM<?php //echo $orderDetails[0]->getFinalPrice();?></td> -->

                                <td><?php echo $pendingMember[$cnt]->getPaymentBankReference();?></td>
                                <td><?php echo $pendingMember[$cnt]->getTrackingNumber();?></td>
                                <td>
                                    <form method="POST" action="utilities/adminRegistrationApprovedFunction.php" class="hover1">
                                    <!-- <form method="POST" action="utilities/adminRegistrationApprovedFunctionTestOne.php" class="hover1"> -->
                                        <button class="clean transparent-button left-button pointer green-to-blue" type="submit" name="uid" value="<?php echo $pendingMember[$cnt]->getUid();?>">
                                            发送
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

</div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>