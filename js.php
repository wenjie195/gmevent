<div class="footer-div">
	<p class="footer-p white-text lato" >© <?php echo $time ;?> 光明日报版权所有</p>
</div>

<!-- Login Modal -->
<div id="login-modal" class="modal-css">

    <!-- Modal content -->
    <div class="modal-content-css login-modal-content">
      <div class="blue-div-opa width100 small-padding">
      	<span class="close-css close-login upper-close">&times;</span>
      	<img src="img/hi.png" class="hello-icon" alt="Welcome Back" title="Welcome Back">
      	<h1 class="white-text welcome lato">Welcome Back!</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     
          <form  method="POST"  action="utilities/loginFunction.php">
    
            <div class="grey-border extra-spacing-bottom"></div>
    
            <p class="input-top-p">Email</p>
            <input class="input-name clean lato blue-text" type="email" placeholder="Email" id="email" name="email" required>
    
            <p class="input-top-p">Password</p>
            <div class="fake-input-div overflow ow-no-margin-bottom">
            	<input class="input-name clean password-input lato blue-text" type="password" placeholder="Password" id="password" name="password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
    		</div>
    		<a class="open-forgot forgot-a blue-link lato">Forgot Password?</a>
            <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="login">Login</button>
    		<p class="signup-p text-center"><a class="open-signup blue-link lato text-center signup-a">No yet have an account? Sign up now!</a></p>
           
    
          </form>
    </div>

</div></div>
<!-- Sign Up Modal -->
<div id="signup-modal" class="modal-css">

    <!-- Modal content -->
    <div class="modal-content-css login-modal-content">
      <div class="blue-div-opa width100 small-padding">
      	<span class="close-css close-signup upper-close">&times;</span>
      	<img src="img/shake-hands.png" class="hello-icon" alt="Glad to See You!" title="Glad to See You!">
      	<h1 class="white-text welcome lato">Glad to See You!</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     
          <form method="POST"  action="utilities/registerFunction.php">
    
            <div class="grey-border extra-spacing-bottom"></div>
			<p class="input-top-p">Full Name</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Full Name" id="register_fullname" name="register_fullname" required>
                
            <p class="input-top-p">Email</p>
            <input class="input-name clean lato blue-text" type="email" placeholder="Email" id="register_email" name="register_email" required>

			<p class="input-top-p">Contact Number</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Contact Number" id="register_contact" name="register_contact" required>

			<p class="input-top-p">Company Name</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="Company Name" id="register_company_name" name="register_company_name" required>
    
            <p class="input-top-p">Password</p>
            <div class="fake-input-div overflow">
            	<input class="input-name clean password-input lato blue-text" type="password" placeholder="Password" id="register_password" name="register_password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
    		</div>
 
            <p class="input-top-p">Retype Password</p>
            <div class="fake-input-div overflow">
            	<input class="input-name clean password-input lato blue-text" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
    		</div> 
    		<input type="checkbox" class="clean agree-size" id="agree" name="" value="agree" required>
  			<label for="agree" class="clean blue-text lato agree-size">I agree to VinCaps' Terms and Conditions and Privacy Policy.</label>
            <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="register">Sign Up</button>
    		<p class="signup-p text-center"><a class="open-login blue-link lato text-center signup-a">Already have an account? Login here.</a></p>
           
    
          </form>
    </div>

</div></div>
<!-- Forgot Modal -->
<div id="forgot-modal" class="modal-css">

    <!-- Modal content -->
    <div class="modal-content-css login-modal-content">
      <div class="blue-div-opa width100 small-padding">
      	<span class="close-css close-forgot upper-close">&times;</span>
      	<img src="img/forgot-password.png" class="hello-icon" alt="Forgot Password?" title="Forgot Password?">
      	<h1 class="white-text welcome lato">Forgot Password?</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     
          <form  method="POST"  action="utilities/loginFunction.php">
    
            <div class="grey-border extra-spacing-bottom"></div>
    
            <p class="input-top-p">Key in Your Email</p>
            <input class="input-name clean lato blue-text" type="email" placeholder="Key in Your Email" id="email" name="email" required>
			
    		
            <button class="input-submit blue-button white-text clean pointer lato below-forgot" name="">Send</button>
    		<p class="signup-p text-center"><a class="open-login blue-link lato text-center signup-a">Login</a></p>
           
    
          </form>
    </div>

</div></div>
<div id="referlink-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referlink">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 text-center ow-no-margin-bottom">What is Article Link/URL?</h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">Article link/URL is the link to your article. It shall not repeat with other article or contain any spacing or symbol such as coma and dot , 'etc. You can use - to separate the text. The more popular and hot keyword used to form the link will help your article get better ranking in Google search result page.</p>
        <img src="img/link.jpg" class="width100" alt="What is Article Link/URL?" title="What is Article Link/URL?">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referlink opacity-hover pointer">Close</div>
     </div>           
	</div>
</div>
<div id="referkeyword-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referkeyword">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 text-center ow-no-margin-bottom">What is Google Keyword?</h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">Google Keyword is the keyword when the user typed to search for the content in Google Search. Google will show the website contains the keyword on top of the search result. You can help user to find this article easier if you key in related keyword. This keyword is only helping the system, it won't appear inside the article or anywhere to be seen. You need to use a coma , to separate each keyword. Example: fundraising, Malaysia, business, tips,</p>
        <img src="img/keyword.jpg" class="width100" alt="What is Google Keyword?" title="What is Google Keyword?">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referkeyword opacity-hover pointer">Close</div>
     </div>           
	</div>
</div>
<div id="referde-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referde">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 text-center ow-no-margin-bottom">Where Article Summary Will Be Shown?</h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">Article summary will be shown at the homepage as short description of the article and inside the Google Search Result. It won't show inside the article content.</p>
        <img src="img/description.jpg" class="width100" alt="Where Article Summary Will Be Shown?" title="Where Article Summary Will Be Shown?">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referde opacity-hover pointer">Close</div>
     </div>           
	</div>
</div>
<div id="refertexteditor-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-refertexteditor">&times;</span>
        <div class="clear"></div>
  
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">How to place image? Click the image icon. Please don't directly copy paste image inside.</p>
        <img src="img/image-icon.jpg" class="width100" alt="How to place image?" title="How to place image?">
    	<p class="step-p step-p2">Right click the image and choose copy image address.</p>
        <img src="img/right-click.jpg" class="width100" alt="How to place image?" title="How to place image?">        
    	<p class="step-p step-p2">Paste the image address inside and click ok.</p>
        <img src="img/paste-image.jpg" class="width100" alt="How to place image?" title="How to place image?"> 
        <h3 class="step-h3">How to Embed Youtube Video?</h3>  
    	<p class="step-p step-p2">Click iframe (the Earth icon) from the tool list.</p>
        <img src="img/earth-icon.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">    	
        <p class="step-p step-p2">Click share below the Youtube video.</p>
        <img src="img/share.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?"> 
        <p class="step-p step-p2">Click embed.</p>
        <img src="img/embed.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">        
        <p class="step-p step-p2">Copy the link ONLY, start from https</p>
        <img src="img/youtube-embed.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">        <p class="step-p step-p2">Paste the link and click ok.</p>
        <img src="img/youtube-embed2.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">        <h3 class="step-h3">How to Embed Facebook Video?</h3>     
        <p class="step-p step-p2">Make sure the video settings is public. Click share and then click embed.</p>
        <img src="img/fb-embed.png" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">        
         <p class="step-p step-p2">Copy the link inside the after src (the highlighted part inside the picture).</p>
        <img src="img/fb-embed2.png" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">         				
        <p class="step-p step-p2">Click iframe (the Earth icon) from the tool list.</p>
        <img src="img/earth-icon.jpg" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">        	
        <p class="step-p step-p2">Paste the link and click ok.</p>
        <img src="img/youtube-embed.jpg" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">        <h3 class="step-h3"><h3 class="step-h3">How to insert link? Click the link/chain icon.</h3>
        <img src="img/paste-link.jpg" class="width100" alt="How to insert link? Click the link/chain icon." title="How to insert link? Click the link/chain icon.">
         <p class="step-p step-p2">Type the display text for the link. Paste the link inside URL column.</p>
        <img src="img/paste-link2.png" class="width100" alt="How to insert link? Click the link/chain icon." title="How to insert link? Click the link/chain icon.">                   
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-refertexteditor opacity-hover pointer">Close</div>
     </div>           
	</div>
</div>

<script src="js/bootstrap.min.js" type="text/javascript"></script> 
<script src="js/wow.min.js" type="text/javascript"></script>
    <script>
     new WOW().init();
    </script>	   
<script src="js/headroom.js"></script>


<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
 <script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>
var loginmodal = document.getElementById("login-modal");
var signupmodal = document.getElementById("signup-modal");
var forgotmodal = document.getElementById("forgot-modal");
var refermodal = document.getElementById("refer-modal");
var referlinkmodal = document.getElementById("referlink-modal");
var refertexteditormodal = document.getElementById("refertexteditor-modal");
var referdemodal = document.getElementById("referde-modal");
var referkeywordmodal = document.getElementById("referkeyword-modal");

var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openrefer = document.getElementsByClassName("open-refer")[0];
var openrefer1 = document.getElementsByClassName("open-refer")[1];
var openrefer2 = document.getElementsByClassName("open-refer")[2];
var openrefer3 = document.getElementsByClassName("open-refer")[3];
var openrefer4 = document.getElementsByClassName("open-refer")[4];
var openreferlink = document.getElementsByClassName("open-referlink")[0];
var openrefertexteditor = document.getElementsByClassName("open-refertexteditor")[0];
var openreferde = document.getElementsByClassName("open-referde")[0];
var openreferkeyword = document.getElementsByClassName("open-referkeyword")[0];

var closelogin = document.getElementsByClassName("close-login")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closerefer = document.getElementsByClassName("close-refer")[0];
var closereferlink = document.getElementsByClassName("close-referlink")[0];
var closerefer1 = document.getElementsByClassName("close-refer")[1];
var closereferlink1 = document.getElementsByClassName("close-referlink")[1];
var closerefertexteditor = document.getElementsByClassName("close-refertexteditor")[0];
var closerefertexteditor1 = document.getElementsByClassName("close-refertexteditor")[1];
var closereferde = document.getElementsByClassName("close-referde")[0];
var closereferde1 = document.getElementsByClassName("close-referde")[1];
var closereferkeyword = document.getElementsByClassName("close-referkeyword")[0];
var closereferkeyword1 = document.getElementsByClassName("close-referkeyword")[1];

if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";
}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";
}
}
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openrefer){
openrefer.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer1){
openrefer1.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer2){
openrefer2.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer3){
openrefer3.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer4){
openrefer4.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openreferlink){
openreferlink.onclick = function() {
  referlinkmodal.style.display = "block";
}
}
if(openrefertexteditor){
openrefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "block";
}
}
if(openreferde){
openreferde.onclick = function() {
  referdemodal.style.display = "block";
}
}
if(openreferkeyword){
openreferkeyword.onclick = function() {
  referkeywordmodal.style.display = "block";
}
}


if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closerefer){
  closerefer.onclick = function() {
  refermodal.style.display = "none";
}
}
if(closerefer1){
  closerefer1.onclick = function() {
  refermodal.style.display = "none";
}
}
if(closereferlink){
  closereferlink.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferlink1){
  closereferlink1.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferkeyword){
  closereferkeyword.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closereferkeyword1){
  closereferkeyword1.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closerefertexteditor){
  closerefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closerefertexteditor1){
  closerefertexteditor1.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closereferde){
  closereferde.onclick = function() {
  referdemodal.style.display = "none";
}
}
if(closereferde1){
  closereferde1.onclick = function() {
  referdemodal.style.display = "none";
}
}


window.onclick = function(event) {
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }  
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == refermodal) {
    refermodal.style.display = "none";
  }   
  if (event.target == referlinkmodal) {
    referlinkmodal.style.display = "none";
  } 
  if (event.target == refertexteditormodal) {
    refertexteditormodal.style.display = "none";
  }
  if (event.target == referdemodal) {
    referdemodal.style.display = "none";
  }        
  if (event.target == referkeywordmodal) {
    referkeywordmodal.style.display = "none";
  }    
}
</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script> 
<script src="js/jquery.ripples.js"></script>
<script>
$(document).ready(function() {
	try {
		
		$('.bottom-water-div').ripples({
			resolution: 412,
			dropRadius: 20, //px
			perturbance: 0.007,
			interactive: true
		});
	}
	catch (e) {
		$('.error').show().text(e);
	}

	$('.js-ripples-disable').on('click', function() {
		$('.bottom-water-div').ripples('destroy');
		$(this).hide();
	});

	$('.js-ripples-play').on('click', function() {
		$('.bottom-water-div').ripples('pause');
	});

	$('.js-ripples-pause').on('click', function() {
		$('.bottom-water-div').ripples('pause');
	});

	// Automatic drops
	setInterval(function() {
		var $el = $('.bottom-water-div');
		var x = Math.random() * $el.outerWidth();
		var y = Math.random() * $el.outerHeight();
		var dropRadius = 20;
		var strength = 0.01 + Math.random() * 0.01;

		$el.ripples('drop', x, y, dropRadius, strength);
	}, 8000);
});
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
function copy() {
  var copyText = document.getElementById("rhb");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>
<script>
function copyUOB() {
  var copyText = document.getElementById("uob");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>

<!-- JS Notice Modal Start-->
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>
<!--<script src="js/progressive-image.js" type="text/javascript"></script> -->
  
<script>
function printFunction() {
    window.print();
}
</script>
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>
<!--   <script src="js/jssor.slider.min.js"></script>
      <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 2000,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 1920;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <script type="text/javascript">jssor_1_slider_init();</script>-->
 <script>
(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 2,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('#count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});


</script>  
<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content ow-notice-width">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <div class="clear"></div>
    <h1 class="dark-blue-text lato text-center notice-title" id="noticeTitle">Title Here</h1>
	<div class="dark-blue-text lato text-center" id="noticeMessage">Message Here</div>
  </div>

</div>
<!-- JS Notice Modal Start End-->
    <script type="text/javascript" src="js/jquery.downCount.js"></script>

    <script class="source" type="text/javascript">
        $('.countdown').downCount({
            date: '13/01/2022 00:00:00',
            offset: +10
        });
    </script>
