<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/gmevent/login.php" />
<link rel="canonical" href="https://vincaps.com/gmevent/login.php" />
<meta property="og:title" content="登入Canva速成班 | 光明日报" />
<title>登入Canva速成班 | 光明日报</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<img src="img/left-leaf.png" class="left-leaf">
<div class="width100 bg3 minheight-with-footer">
    <div class="big-container-css width100 ow-big-container-css">
      <div class="blue-div-opa width100 small-padding">
      	<!--<span class="close-css inner-close  upper-close">&times;</span>-->
      	<img src="img/hi.png" class="hello-icon blue-icon2" alt="Login" title="Login">
      	<h1 class="white-text welcome lato welcome2">登入</h1>
      </div>
      <div class="white-bg width100 small-padding overflow below-blue-box">
     <form  method="POST"  action="utilities/loginFunction.php">
    
            <div class="grey-border extra-spacing-bottom"></div>
    
            <!-- <p class="input-top-p">Email</p>
            <input class="input-name clean lato blue-text" type="email" placeholder="Email" id="email" name="email" required> -->

            <p class="input-top-p">用户名</p>
            <input class="input-name clean lato blue-text" type="text" placeholder="用户名" id="username" name="username" required>
    
            <p class="input-top-p">密码</p>
            <div class="fake-input-div overflow ow-no-margin-bottom">
            	<input class="input-name clean password-input lato blue-text" type="password" placeholder="密码" id="password" name="password" required>
                <img src="img/eye.png" class="opacity-hover pointer eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
    		</div>
    		<!--<a class="open-forgot forgot-a blue-link lato">Forgot Password?</a>-->
            <button class="input-submit green-bg-change white-text clean pointer lato below-forgot text-center width100 border0" name="login">登入</button>
    		<!--<p class="signup-p text-center"><a class="open-signup blue-link lato text-center signup-a">No yet have an account? Sign up now!</a></p>-->
           
    
          </form>


    </div>
  </div>
 </div>
<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}

</style>







<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "message 2"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>