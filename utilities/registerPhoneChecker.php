<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$mobile = $_POST['mobile'];
$userDetails = getOrders($conn, "WHERE phone =?",array("phone"),array($mobile), "s");
if (empty($userDetails))
{
  $alert = "此联络号码可以注册。";
  $alertBin = 0;
}
else
{
  $alert = "检测到此联络号码已经注册，请使用其他号码。";
  $alertBin = 1;
}

$result[] = array("result" => $alert, "alertBin" => $alertBin);
echo json_encode($result);
?>