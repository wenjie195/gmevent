<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["uid"]);

    $status = "Approved";

    $registrationDetails = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");   

    if($registrationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminRegistrationApproved.php?type=1');
            header('Location: ../adminRegistrationApproved.php');
        }
        else
        {
            echo "FAIL";
        }
    }
    else
    {
        echo "ERROR"; 
    }

}
else 
{
    header('Location: ../index.php');
}
?>