<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];


function sendEmailForVerification($uid)
{
    $conn = connDB();
    $userRows = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    $verifyUser_debugMode = 2;
    $verifyUser_host = "mail.vincaps.com";
    $verifyUser_usernameThatSendEmail = "noreply@vincaps.com";                   // Sender Acc Username
    $verifyUser_password = "8dQLllnG1yh5";                                              // Sender Acc Password

    $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
    $verifyUser_port = 465;                                                              // SMTP port no
    $verifyUser_sentFromThisEmailName = "noreply@vincaps.com";                    // Sender Username
    $verifyUser_sentFromThisEmail = "noreply@vincaps.com";                        // Sender Email


    $verifyUser_sendToThisEmailName = $userRows[0]->getName();                            // Recipient Username
    $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
    $verifyUser_isHtml = true;                                                                // Set To Html
    $verifyUser_subject = "Guang Ming Online Training Registration Confirmation";

    $verifyUser_body = "<p>Hello ".$userRows[0]->getName()."! Thanks for registering our 2-hour online design course!</p>";    // Body
    $verifyUser_body .="<p>Date: 25 Feb 2022 (Friday)</p>";
    $verifyUser_body .="<p>Time: 8-10pm</p>";
    $verifyUser_body .="<p>Location: Zoom</p>";
    $verifyUser_body .="<p>Zoom Link : https://us02web.zoom.us/j/85285340142?pwd=ZUNxWS90Zm16QWV0by9VWTdScG9ZUT09</p>";
    $verifyUser_body .="<p>Meeting ID: 852 8534 0142</p>";
    $verifyUser_body .="<p>Passcode: 088032</p>";
    $verifyUser_body .="<p>Zoom Code : ".$userRows[0]->getTrackingNumber()."</p>";
    $verifyUser_body .="<p>Please click the link above, fill in the provided Zoom Code and your name for registering the class in advance, we will have class right here that day</p>";
    $verifyUser_body .="<p>Attention</p>";
    $verifyUser_body .="<p>Write the Zoom Code at front of your name </p>"; 
    $verifyUser_body .="<p>Example: Zoom Code (your name)</p>";
    $verifyUser_body .="<p>Important Reminder</p>";    
    $verifyUser_body .="<p>We will approve registrations based on the name you provided when you registered for the course. So, please make sure you fill in the same name.</p>";
    $verifyUser_body .="<p>In addition, you are invited to join our Design Learning Group for discussing, sharing and getting more updated information.</p>";
    $verifyUser_body .="<p> >>> Telegram Link: https://t.me/+lATrsFS66cExYmU1 </p>";
    $verifyUser_body .="<p> >>> Facebook Link: https://www.facebook.com/groups/2822103964746671/ </p>";
    $verifyUser_body .="<p>If you have any friends who interested in learning designing post, you can share the link below and invite them to join with you together learning new stuff
                        https://vincaps.com/gmevent2/ </p>";
    $verifyUser_body .="<p>Any questions? Just click 'Reply' and we are glad to help.</p>";
    $verifyUser_body .="<p>See you in the coming days!</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["uid"]);

    $status = "Approved";

    $registrationDetails = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");   

    if($registrationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminRegistrationApproved.php?type=1');
            sendEmailForVerification($uid);
            header('Location: ../adminRegistrationApproved.php');
        }
        else
        {
            echo "FAIL";
        }
    }
    else
    {
        echo "ERROR"; 
    }

}
else 
{
    header('Location: ../index.php');
}
?>