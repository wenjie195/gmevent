<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];


function sendEmailForVerification($uid)
{
    $conn = connDB();
    $userRows = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    $verifyUser_debugMode = 2;
    $verifyUser_host = "mail.vincaps.com";
    $verifyUser_usernameThatSendEmail = "noreply@vincaps.com";                   // Sender Acc Username
    $verifyUser_password = "8dQLllnG1yh5";                                              // Sender Acc Password

    $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
    $verifyUser_port = 465;                                                              // SMTP port no
    $verifyUser_sentFromThisEmailName = "noreply@vincaps.com";                    // Sender Username
    $verifyUser_sentFromThisEmail = "noreply@vincaps.com";                        // Sender Email


    $verifyUser_sendToThisEmailName = $userRows[0]->getName();                            // Recipient Username
    $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
    $verifyUser_isHtml = true;                                                                // Set To Html
    $verifyUser_subject = "GM Event Registration Confirmation";

    $verifyUser_body = "<p>Dear ".$userRows[0]->getName().",</p>";                      // Body
    $verifyUser_body .="<p>Thanks you for your registration, below is the event details :</p>";
    $verifyUser_body .="<p>FB Group Link: https://www.facebook.com/groups/2822103964746671/</p>";
    $verifyUser_body .="<p>Telegram Group Link: https://t.me/+xTjS6uwTZO4zZGE1</p>";
    $verifyUser_body .="<p>Thanks you for your registration, below is the event details :</p>";
    $verifyUser_body .="<p>若有疑问可拨打+60 16-418 3692</p>  ";
    $verifyUser_body .="<p>Thank you.</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["uid"]);

    $status = "Approved";

    $registrationDetails = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");   

    if($registrationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminRegistrationApproved.php?type=1');
            sendEmailForVerification($uid);
            header('Location: ../adminRegistrationApproved.php');
        }
        else
        {
            echo "FAIL";
        }
    }
    else
    {
        echo "ERROR"; 
    }

}
else 
{
    header('Location: ../index.php');
}
?>