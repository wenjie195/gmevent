<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
// require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewProduct($conn,$uid,$name,$price,$description,$imageOne,$status)
{
     if(insertDynamicData($conn,"product",array("uid","name","price","description","image_one","status"),
          array($uid,$name,$price,$description,$imageOne,$status),"ssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $category= rewrite($_POST['register_category']);
     // $brand = rewrite($_POST['register_brand']);

     $category = "Hydrogen";
     $brand = "Hydrogen";

     $name = rewrite($_POST['name']);
     $description = rewrite($_POST['description']);
     $price = rewrite($_POST['price']);

     $status = "Available";

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     // $target_dir = "../productImage/";
     $target_dir = "../img/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $productNameRows = getProduct($conn," WHERE name = ? ",array("name"),array($name),"s");
     $existingProductName = $productNameRows[0];

     if(!$existingProductName)
     {
          if(registerNewProduct($conn,$uid,$name,$price,$description,$imageOne,$status))
          {
               // echo "SUCCESS 2";
               $_SESSION['messageType'] = 3;
               header('Location: ../adminProductAll.php?type=1');
          }
          else
          {
               // echo "FAIL";
               $_SESSION['messageType'] = 3;
               header('Location: ../adminProductAll.php?type=2');
          }
     }
     else
     {
          // echo "ERROR";
          $_SESSION['messageType'] = 3;
          header('Location: ../adminProductAll.php?type=3');
     }
}
else
{
     header('Location: ../index.php');
}
?>