<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];


function sendEmailForVerification($uid)
{
    $conn = connDB();
    $userRows = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    $verifyUser_debugMode = 2;
    $verifyUser_host = "mail.vincaps.com";
    $verifyUser_usernameThatSendEmail = "noreply@vincaps.com";                   // Sender Acc Username
    $verifyUser_password = "8dQLllnG1yh5";                                              // Sender Acc Password

    $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
    $verifyUser_port = 465;                                                              // SMTP port no
    $verifyUser_sentFromThisEmailName = "noreply@vincaps.com";                    // Sender Username
    $verifyUser_sentFromThisEmail = "noreply@vincaps.com";                        // Sender Email


    $verifyUser_sendToThisEmailName = $userRows[0]->getName();                            // Recipient Username
    $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
    $verifyUser_isHtml = true;                                                                // Set To Html
    $verifyUser_subject = "Intensive Canva Class";

    // $verifyUser_body = "<p>你好, 我们已收到你报名我们的 Canva速成班 2小时在线课程！</p>";           // Body
    // $verifyUser_body .="<p>日期： 13 Jan 2022 (星期四）</p>";
    // $verifyUser_body .="<p>时间：-	</p>";
    // $verifyUser_body .="<p>地点：  Zoom</p>";
    // $verifyUser_body = "<p>以下是课程当天 Zoom Code</p>";
    // $verifyUser_body .="<p>".$userRows[0]->getTrackingNumber()."</p>";

    $verifyUser_body = 
    "<p>
        你好！我们已收到你报名我们的【Canva速成班】 2 小时在线课程！
        <br>
        日期： 13 Jan 2022 (星期四）
        <br>
        地点：  Zoom
        <br>
        Zoom Code: ".$userRows[0]->getTrackingNumber()."
    </p>"; 

    $verifyUser_body .="<p>注册Link：</p>";
    $verifyUser_body .="<p>请你点击 以上的Link , 填写email注册先，当晚我们就在这里上课👋🏻</p>";
    $verifyUser_body .="<p>⚠️重要提醒⚠️</p>";
    $verifyUser_body .="<p>我们将根据你报名课程时提供的email批准注册申请。所以，请确保你填写相同的email注册。</p>";
    $verifyUser_body .="<p>🚩此外，加入我们的【设计学习交流群】与同学们一起讨论，分享，获得更多的新资讯 </p>";
    $verifyUser_body .="<p> >>> Telegram Link: https://t.me/+xTjS6uwTZO4zZGE1 </p>";
    $verifyUser_body .="<p> >>> Facebook Link: https://www.facebook.com/groups/2822103964746671/ </p>";
    $verifyUser_body .="<p>如果你有任何感兴趣的朋友，可以邀请他们加入你一起学习，请点击以下链接：</p>";
    $verifyUser_body .="<p>https://vincaps.com/gmevent2/ </p>";
    $verifyUser_body .="<p>有任何问题？直接点击‘Reply’, 我们很乐意提供帮助！</p>";
    $verifyUser_body .="<p>当天见，感谢你的报名！</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["uid"]);

    $status = "Approved";

    $registrationDetails = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");   

    if($registrationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminRegistrationApproved.php?type=1');
            sendEmailForVerification($uid);
            header('Location: ../adminRegistrationApproved.php');
        }
        else
        {
            echo "FAIL";
        }
    }
    else
    {
        echo "ERROR"; 
    }

}
else 
{
    header('Location: ../index.php');
}
?>