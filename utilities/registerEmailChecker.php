<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$email = $_POST['email'];
$userDetails = getOrders($conn, "WHERE email =?",array("email"),array($email), "s");
if (empty($userDetails))
{
  $alert = "此电邮可以注册。";
  $alertBin = 0;
}
else
{
  $alert = "此电邮已经注册，请输入其他电邮。";
  $alertBin = 1;
}

$result[] = array("result" => $alert, "alertBin" => $alertBin);
echo json_encode($result);
?>