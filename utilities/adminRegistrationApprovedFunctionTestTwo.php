<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];


function sendEmailForVerification($uid)
{
    $conn = connDB();
    $userRows = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    $verifyUser_debugMode = 2;
    $verifyUser_host = "mail.vincaps.com";
    $verifyUser_usernameThatSendEmail = "noreply@vincaps.com";                   // Sender Acc Username
    $verifyUser_password = "8dQLllnG1yh5";                                              // Sender Acc Password

    $verifyUser_smtpSecure = "ssl";                                                      // SMTP type
    $verifyUser_port = 465;                                                              // SMTP port no
    $verifyUser_sentFromThisEmailName = "noreply@vincaps.com";                    // Sender Username
    $verifyUser_sentFromThisEmail = "noreply@vincaps.com";                        // Sender Email


    $verifyUser_sendToThisEmailName = $userRows[0]->getName();                            // Recipient Username
    $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
    $verifyUser_isHtml = true;                                                                // Set To Html
    $verifyUser_subject = "【Canva速成班】即将开始 温馨提醒";

    $verifyUser_body = "<p>【Canva速成班】即将开始 温馨提醒</p>";                      // Body
    $verifyUser_body .="<p>明天就是了！</p>";
    $verifyUser_body .="<p>如果你还没注册进课程的link，可以现在注册了，只需填写email</p>";
    $verifyUser_body .="<p>注册网址：</p>";
    $verifyUser_body .="<p>⚠️重要提醒⚠️</p>";
    $verifyUser_body .="<p>我们将根据你报名课程时提供的email批准注册申请。所以，请确保你填写相同的email注册。</p>  ";
    $verifyUser_body .="<p>如果进不到link的话， 跟我们说哦， 我们会尽快帮你处理。</p>";
    $verifyUser_body .="<p>别错过出席课程，明天见！</p>";

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port,
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["uid"]);

    $status = "Approved";

    $registrationDetails = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");   

    if($registrationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $membershipUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($membershipUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminRegistrationApproved.php?type=1');
            sendEmailForVerification($uid);
            header('Location: ../adminRegistrationApproved.php');
        }
        else
        {
            echo "FAIL";
        }
    }
    else
    {
        echo "ERROR"; 
    }

}
else 
{
    header('Location: ../index.php');
}
?>