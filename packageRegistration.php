<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();

//     // $uid = md5(uniqid());
//     // $name = rewrite($_POST["name"]);
//     // $mobile = rewrite($_POST["mobile"]);
//     // $email = rewrite($_POST["email"]);

//     // $timestamp = time();
//     // $_SESSION['timestamp'] = $timestamp;

//     $productName = rewrite($_POST["product_name"]);
//     $productPrice = rewrite($_POST["product_price"]);    

//     // $priceRenew = (($productPrice) * 100);

// }

$conn = connDB();

$currentPackage = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
$packageDetails = $currentPackage[0];


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/packageRegistration.php" />
<link rel="canonical" href="https://vincaps.com/packageRegistration.php" />
<meta property="og:title" content="Registration" />
<title>Registration</title>

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1" style="padding-top:70px !important;">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn" data-wow-delay="0.2s">Fill Details</h1>

        <div class="five-col float-left two-column-css">

        <!-- <form method="post" action="billplzpost.php"> -->
        <form method="post" action="packageConfirmation.php">

            <input type="text" value="User 1" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" required>
            <a id="mobileAlert"></a>
            <input type="text" type="text" value="0111231231" id="mobile" name="mobile" class="input-name clean dark-blue-text wow fadeIn" required>
            <a id="emailAlert"></a>
            <input type="email" value="user1@gmail.com" id="email" name="email" class="input-name clean dark-blue-text wow fadeIn" required>
            <a id="emailMatchAlert"></a>
            <input type="email" value="user1@gmail.com" id="reEmail" name="reEmail" class="input-name clean dark-blue-text wow fadeIn" required>

            
            <!-- <input type="text" placeholder="Name" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" required>
            <a id="mobileAlert"></a>
            <input type="text" placeholder="Mobile Number" id="mobile" name="mobile" class="input-name clean dark-blue-text wow fadeIn" required>
            <a id="emailAlert"></a>
            <input type="email" placeholder="Email" id="email" name="email" class="input-name clean dark-blue-text wow fadeIn" required>
            <a id="emailMatchAlert"></a>
            <input type="email" placeholder="Retype Email" id="reEmail" name="reEmail" class="input-name clean dark-blue-text wow fadeIn" required> -->
        
            <!-- <Package Selected  -->
            <!-- <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="<?php //echo $productName;?>" id="item_name" name="item_name" readonly> 
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="<?php //echo $productPrice;?>" id="item_price" name="item_price" readonly>  -->

            <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="<?php echo $packageDetails->getName();;?>" id="item_name" name="item_name" readonly> 
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" value="<?php echo $packageDetails->getPrice();;?>" id="item_price" name="item_price" readonly> 

            <div class="res-div">
                <input type="submit" name="submit" value="Proceed To Payment" class="input-submit blue-button white-text clean pointer lato wow fadeIn" style="margin-bottom:30px;">
            </div>
        </form>
            
        </div>	
</div>

<div class="clear"></div>  

<!-- <div class="width100 overflow">
</div> -->

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>

<?php include 'js.php'; ?>

<?php unset($_SESSION['registration_uid']);?>

<script type="text/javascript">
    $("#reEmail").on("keyup",function(){
        // alert("SS");
        var email = $("#email").val();
        var reEmail = $(this).val();
        if (email == reEmail) {
            $("#emailMatchAlert").text("Email Match");
            $("#emailMatchAlert").css({
                "color":"green",
                "font-size":"12px",
            });
            $("#submit").prop("disabled",false);
        }else{
            $("#emailMatchAlert").text("Email and Retype Email are different !");
            $("#emailMatchAlert").css({
              "color":"red",
              "font-size":"12px",
              "font-weight": "bold",
              "float": "right",
            });
            $("#submit").prop("disabled",true);
        }
    });

    $("#mobile").on("keyup",function(){
      var mobile = $(this).val();
      $.ajax({
        url: 'utilities/registerPhoneChecker.php',
        data: {mobile:mobile},
        type: 'post',
        dataType: 'json',
        success:function(response){
          var resultAlert = response[0]['result'];
          var alertBin = response[0]['alertBin'];
          if (alertBin == 0 && mobile) {
          $("#mobileAlert").text(resultAlert);
            $("#mobileAlert").css({
                "color":"green",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "right",
            });
            $("#submit").prop("disabled",false);
          }else if(alertBin == 1 && mobile){
          $("#mobileAlert").text(resultAlert);
            $("#mobileAlert").css({
                "color":"red",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "right",
            });
            $("#submit").prop("disabled",true);
          }else{
            $("#mobileAlert").empty();
          }
        },
      });
    });

    $("#email").on("keyup",function(){
      var email = $(this).val();
      $.ajax({
        url: 'utilities/registerEmailChecker.php',
        data: {email:email},
        type: 'post',
        dataType: 'json',
        success:function(response){
          var resultAlert = response[0]['result'];
          var alertBin = response[0]['alertBin'];
          if (alertBin == 0 && email) {
          $("#emailAlert").text(resultAlert);
            $("#emailAlert").css({
                "color":"green",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "right",
            });
            $("#submitBtn").prop("disabled",false);
          }else if(alertBin == 1 && email){
          $("#emailAlert").text(resultAlert);
            $("#emailAlert").css({
                "color":"red",
                "font-size":"12px",
                "font-weight": "bold",
                "float": "right",
            });
            $("#submitBtn").prop("disabled",true);
          }else{
            $("#emailAlert").empty();
          }
        },
      });
    });
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        elseif($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Mobile Number or Email already existing, Pls try again !";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>