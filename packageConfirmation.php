<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = md5(uniqid());

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = md5(uniqid());

    $name = rewrite($_POST["name"]);
    $mobile = rewrite($_POST["mobile"]);
    $email = rewrite($_POST["email"]);

    $productName = rewrite($_POST["item_name"]);
    $productPrice = rewrite($_POST["item_price"]);    

    $priceAdjust = (($productPrice) * 100);

    $timestamp = time();
    $_SESSION['timestamp'] = $timestamp;
    $orderID = $timestamp;

    $newstring = substr($uid, 1,5);

    $trackingId = $newstring;

    function addOrders($conn,$uid,$orderID,$name,$mobile,$email,$trackingId)
    {
        if(insertDynamicData($conn,"orders",array("uid","order_id","name","phone","email","tracking_number"),
        array($uid,$orderID,$name,$mobile,$email,$trackingId),"ssssss") === null)
        {
            echo "gg";
        }
        else{    }
        return true;
    }

    function addOrdersList($conn,$uid,$orderID,$productName,$productPrice)
    {
        if(insertDynamicData($conn,"order_list",array("user_uid","order_id","product_name","final_price"),
        array($uid,$orderID,$productName,$productPrice),"ssss") === null)
        {
            echo "gg";
        }
        else{    }
        return true;
    }

    // if(addOrders($conn,$uid,$orderID,$name,$mobile,$email))
    // {
    //     if(addOrdersList($conn,$uid,$orderID,$productName,$productPrice))
    //     {}
    // }

    $mobileRows = getOrders($conn," WHERE phone = ? ",array("phone"),array($mobile),"s");
    $existingMobile = $mobileRows[0];

    $emailRows = getOrders($conn," WHERE email = ? ",array("email"),array($email),"s");
    $existingEmail = $emailRows[0];

    if(!$existingMobile && !$existingEmail)
    {
        if(addOrders($conn,$uid,$orderID,$name,$mobile,$email,$trackingId))
        {
            if(addOrdersList($conn,$uid,$orderID,$productName,$productPrice))
            {}
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ./index.php#register?type=3');
        // header('Location: ./package.php?type=3');
    }

}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/gmevent/packageRegistration.php" />
<link rel="canonical" href="https://vincaps.com/gmevent/packageRegistration.php" />
<meta property="og:title" content="确认报名文案图设计速成班 | 光明日报" />
<title>确认报名文案图设计速成班 | 光明日报</title>
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
	<img src="img/right-leaf.png" class="right-leaf">
    <div class="clear"></div>
    <div class="width100 same-padding menu-margin-top min-height-with-margin">
		<h1 class="line-header wow fadeIn" data-wow-delay="0.2s">请确认资料无误</h1> 
        <div class="input-div-width">

        <form method="post" action="billplzpost.php">
        <!-- <form method="post" action="packageConfirmation.php"> -->
			<p class="top-p darkblue-text">姓名</p>
            <input type="text" value="<?php echo $name;?>" id="name" name="name" class="input-name clean dark-blue-text wow fadeIn" readonly ><p class="top-p darkblue-text">联络号码</p>
            <input type="text" type="text" value="<?php echo $mobile;?>" id="mobile" name="mobile" class="input-name clean dark-blue-text wow fadeIn" readonly >
            <p class="top-p darkblue-text">电邮</p>
            <input type="email" value="<?php echo $email;?>" id="email" name="email" class="input-name clean dark-blue-text wow fadeIn" readonly >
        

            <input class="input-name clean dark-blue-text wow fadeIn" type="text" id="amount" name="amount" value="<?php echo $priceAdjust;?>" readonly style="display:none;"> 

            <input class="input-name clean dark-blue-text wow fadeIn" type="text" id="reference_1_label" name="reference_1_label" value="Product" readonly style="display:none;"> 
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" id="reference_1" name="reference_1" value="<?php echo $productName;?>" readonly style="display:none;">
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" id="reference_2_label" name="reference_2_label" value="Order ID" readonly style="display:none;"> 
            <input class="input-name clean dark-blue-text wow fadeIn" type="text" id="reference_2" name="reference_2" value="<?php echo $orderID;?>" readonly style="display:none;"> 

            <div class="res-div">
                <input type="submit" name="submit" value="前往付款" class="width100-ow green-bg-change middle-button white-text wow fadeIn clean" style="margin-bottom:30px;"></div>
        </form>
            
        </div>	
</div>

<div class="clear"></div>  



<?php include 'js.php'; ?>
<?php unset($_SESSION['registration_uid']);?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "已成功注册，请在近几日内留意你的电子邮箱。"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "电邮或联络号码已被注册，请尝试其他电邮或号码。"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "message 3";
        }
        echo '
        <script>
            putNoticeJavascript("","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>