<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';


require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$products = getProduct($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/package.php" />
<link rel="canonical" href="https://vincaps.com/package.php" />
<meta property="og:title" content="Package" />
<title>Package</title>

<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 overflow teh2-div same-padding  padding-top-bottom2" id="register1" style="padding-top:70px !important;">
	<h1 class="teh-all-h1 text-center dark-blue-text wow fadeIn">Package Type</h1>

        <div class="five-col float-left two-column-css">

        <!-- <form method="post" action="dataConfirmation.php"> -->
        <!-- <form method="post" action="#"> -->

            <div class="scroll-div margin-top30">
                <table class="approve-table lato">
                    <thead>
                    <tr>
                        <td>Product Name</td>
                        <td>Price</td>
                        <td>Option Select</td>
                    </tr>
                    </thead>

                    <tbody>
                    
                    <?php
                    if($products)
                    {
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo $products[$cnt]->getName();?></td>
                                <td>RM <?php echo $products[$cnt]->getPrice();?></td>

                                <td>
                                    <!-- <form method="POST" action="utilities/adminSubscribeApprovedFunction.php" class="hover1"> -->
                                    <form method="POST" action="packageRegistration.php" class="hover1">

                                        <input type="hidden" value="<?php echo $products[$cnt]->getName();?>" id="product_name" name="product_name" readonly>
                                        <input type="hidden" value="<?php echo $products[$cnt]->getPrice();?>" id="product_price" name="product_price" readonly>

                                        <button class="clean transparent-button left-button" type="submit">
                                            <img src="img/approve.png" class="approval-icon" alt="Select" title="Select">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>

                    </tbody>	
                </table>
            </div>


            <!-- <div class="res-div">
                <input type="submit" name="submit" value="Continue" class="input-submit blue-button white-text clean pointer lato wow fadeIn" style="margin-bottom:30px;">
            </div> -->

        <!-- </form> -->
            
        </div>	
</div>

<div class="clear"></div>  

<!-- <div class="width100 overflow">
</div> -->

<style>
.footer-div{
    bottom: 0;
    position: fixed;
    width: 100%;}
</style>

<?php include 'js.php'; ?>

<?php unset($_SESSION['registration_uid']);?>
<?php unset($_SESSION['timestamp']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "REGISTER SUCCESSFULLY ! <br> PLEASE CHECK YOUR MAILBOX FOR THE VOUCHER !"; 
        }
        elseif($_GET['type'] == 2)
        {
            // $messageType = "message 2"; 
            $messageType = "Email or Phone Number has been used !<br> Please Retry"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Mobile Number or Email already existing, Pls try again !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';
        // echo '
        // <script>
        //     putNoticeJavascript("","'.$messageType.'");  
        // </script>
        // ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>