<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype'] == 0)
    //admin
    {
    ?>
        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/guangming.png" class="logo-img opacity-hover" alt="光明日报" title="光明日报"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                  
                        <a href="adminUserAll.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            特殊会员
                        </a> 
                        <a href="adminRegistrationPending.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            未发邮件
                        </a>     
                        <a href="adminRegistrationApproved.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            已发邮件
                        </a>                                                           
                        <a href="logout.php" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            登出
                        </a>  
                            	
                    </div>
                </div>
        </header>
    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    //user
    {
    ?>

    <?php
    }
    elseif($_SESSION['usertype'] == 2)
    //premium user
    {
    ?>

    <?php
    }
    ?>

<?php
}
else
{
?>
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"><img src="img/guangming.png" class="logo-img opacity-hover" alt="光明日报" title="光明日报"></a>
                </div>

                <div class="right-menu-div float-right before-header-div">

                        <a href="#register" class="menu-margin-right menu-item lato" style="display:inline-block !important;">
                            注册
                        </a>   
                                            
                </div>
            </div>
    </header>
<?php
}
?>