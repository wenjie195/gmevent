<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://vincaps.com/gmevent/img/canva-fb-meta.jpg" />
<meta name="author" content="光明日报">
<meta property="og:description" content="2小时 0基础轻松学设计 菜鸟也能学会设计文案图。" />
<meta name="description" content="2小时 0基础轻松学设计 菜鸟也能学会设计文案图。" />
<meta name="keywords" content="canva,设计课程,光明日报,guang ming,">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WXV0N1TQF0"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-WXV0N1TQF0');
</script>
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>